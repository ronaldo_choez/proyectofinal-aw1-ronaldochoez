function iniciarMap(){
    var coord = {lat:-0.9668849 , lng: -80.710596};
    var map = new google.maps.Map(document.getElementById('map'),{
      zoom: 20,
      center: coord
    });
    var marker = new google.maps.Marker({
      position: coord,
      map: map
    });
}